-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
   https://w3c.github.io/manifest/#dom-manifestimageresource
-}
let DisplayMode
    : Type
    = < fullscreen | standalone | minimal-ui | browser >

in  { Type = DisplayMode, default = DisplayMode.browser }
