-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/manifest/
-}
let DisplayMode =
        ./DisplayMode.dhall
          sha256:ea8a625a3f2bd2a98b30dc59b4901279c4fc6804f07a11c94dd446d5d691b3e3
      ? ./DisplayMode.dhall

let ExternalApplicationResource =
        ./ExternalApplicationResource.dhall
          sha256:23a483d49d5a400385ec19b208adc890414357de7d3a5e55156caa93360da656
      ? ./DisplayMode.dhall

let ImageResource =
        ./ImageResource.dhall
          sha256:dff7b54e0d5e1df2764887b610300f2f963652b635a8e510d3ebf4993c96d161
      ? ./ImageResource.dhall

let ManifestImageResource =
        ./ManifestImageResource.dhall
          sha256:dcef719e5f60c03ac4e72231de75e4eaea863bfd6734d887cc9d68326aebd017
      ? ./ManifestImageResource.dhall

let OrientationLock =
        ./OrientationLock.dhall
          sha256:a91569d276cf93f6c69760791d4833057db03872f661a8811f7a1b59358a1b64
      ? ./OrientationLock.dhall

let ShareTarget =
        ./ShareTarget.dhall
          sha256:9b41f5a412661b0ba06268e378054b3249202b0894646bc7bd4024127cb32a51
      ? ./ShareTarget.dhall
          sha256:9b41f5a412661b0ba06268e378054b3249202b0894646bc7bd4024127cb32a51

let ShortcutItem =
        ./ShortcutItem.dhall
          sha256:a62fc5ff948880eb10639888567a54cc20df6e9f0d98e7edfadd940351748ef5
      ? ./ShortcutItem.dhall

let TextDirection =
        ./TextDirection.dhall
          sha256:aa6e5b2d0c222d043821cd16c5a249e2d045b3e920e55a9cde06b6951f32d24f
      ? ./TextDirection.dhall

let WebAppManifest =
      { dir : Optional TextDirection.Type
      , lang : Optional Text
      , name : Text
      , short_name : Optional Text
      , description : Optional Text
      , icons : List ManifestImageResource.Type
      , screenshots : Optional (List ImageResource.Type)
      , categories : Optional (List Text)
      , iarc_rating_id : Optional Text
      , start_url : Optional Text
      , display : Optional DisplayMode.Type
      , orientation : Optional OrientationLock.Type
      , theme_color : Optional Text
      , background_color : Optional Text
      , scope : Optional Text
      , related_applications : Optional (List ExternalApplicationResource.Type)
      , prefer_related_applications : Optional Bool
      , shortcuts : Optional (List ShortcutItem.Type)
      , share_target : Optional ShareTarget.Type
      }

let default =
      { dir = None TextDirection.Type
      , lang = None Text
      , short_name = None Text
      , description = None Text
      , icons = [] : List ManifestImageResource.Type
      , screenshots = None (List ImageResource.Type)
      , categories = None (List Text)
      , iarc_rating_id = None Text
      , start_url = None Text
      , display = None DisplayMode.Type
      , orientation = None OrientationLock.Type
      , theme_color = None Text
      , background_color = None Text
      , scope = None Text
      , related_applications = None (List ExternalApplicationResource.Type)
      , prefer_related_applications = None Bool
      , shortcuts = None (List ShortcutItem.Type)
      , share_target = None ShareTarget.Type
      }

in  { Type = WebAppManifest
    , default
    , DisplayMode
    , ExternalApplicationResource
    , ImageResource
    , ManifestImageResource
    , OrientationLock
    , ShareTarget
    , ShortcutItem
    , TextDirection
    }
