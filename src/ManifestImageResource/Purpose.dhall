-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/manifest/#purpose-member
-}
let Text/concatMapSep =
      https://prelude.dhall-lang.org/v20.1.0/Text/concatMapSep.dhall sha256:c272aca80a607bc5963d1fcb38819e7e0d3e72ac4d02b1183b1afb6a91340840

let Purpose
    : Type
    = < monochrome | maskable | any >

let show
    : Purpose → Text
    = λ(purpose : Purpose) →
        merge
          { monochrome = "monochrome", maskable = "maskable", any = "any" }
          purpose

let concatShow
    : List Purpose → Text
    = Text/concatMapSep " " Purpose show

let example0 =
      assert : concatShow [ Purpose.maskable, Purpose.any ] ≡ "maskable any"

in  { Type = Purpose, show, concatShow }
