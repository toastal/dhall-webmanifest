-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/web-share-target/
-}
let Params =
      { Type =
          { title : Optional Text, text : Optional Text, url : Optional Text }
      , default = { title = None Text, text = None Text, url = None Text }
      }

let ShareTarget
    : Type
    = { action : Text
      , method : Optional Text
      , enctype : Optional Text
      , params : Params.Type
      }

let default = { method = None Text, enctype = None Text }

in  { Type = ShareTarget, default, Params }
