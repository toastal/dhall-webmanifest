-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://www.w3.org/TR/image-resource/#dom-imageresource
-}
let ImageResource
    : Type
    = { src : Text, sizes : Optional Text, type : Optional Text }

let default = { sizes = None Text, type = None Text }

in  { Type = ImageResource, default }
