-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/manifest/#dom-manifestimageresource
-}
let ImageResource =
        ./ImageResource.dhall
          sha256:dff7b54e0d5e1df2764887b610300f2f963652b635a8e510d3ebf4993c96d161
      ? ./ImageResource.dhall

let Purpose =
        ./ManifestImageResource/Purpose.dhall
          sha256:5df6c24a4c2ec52a7df18a01accf5bce3febc7af31b1de6ea50fe108effce4f1
      ? ./ManifestImageResource/Purpose.dhall

let ManifestImageResource
    : Type
    = ImageResource.Type ⩓ { purpose : Optional Text }

let default = ImageResource.default ⫽ { purpose = None Text }

in  { Type = ManifestImageResource, default, ImageResource, Purpose }
