-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/manifest/#dom-externalapplicationresource
  An ExternalApplicationResource MUST have either an url or an id (or both).
-}
let Fingerprint
    : Type
    = { type : Text, value : Text }

let ExternalApplicationResource
    : Type
    = { platform : Text
      , url : Optional Text
      , id : Optional Text
      , min_version : Optional Text
      , fingerprints : Optional (List Fingerprint)
      }

let default =
      { url = None Text
      , id = None Text
      , min_version = None Text
      , fingerprints = None (List Fingerprint)
      }

in  { Type = ExternalApplicationResource, Fingerprint, default }
