-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://w3c.github.io/manifest/#dom-textdirectiontype
-}
let TextDirection
    : Type
    = < ltr | rtl | auto >

in  { Type = TextDirection, default = TextDirection.auto }
