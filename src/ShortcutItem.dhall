-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest

{-
  https://w3c.github.io/manifest/#dom-shortcutitem
-}
let ManifestImageResource =
        ./ManifestImageResource.dhall
          sha256:dcef719e5f60c03ac4e72231de75e4eaea863bfd6734d887cc9d68326aebd017
      ? ./ManifestImageResource.dhall

let ShortcutItem
    : Type
    = { name : Text
      , short_name : Optional Text
      , description : Optional Text
      , url : Text
      , icons : Optional (List ManifestImageResource.Type)
      }

let default =
      { short_name = None Text
      , description = None Text
      , icons = None (List ManifestImageResource.Type)
      }

in  { Type = ShortcutItem, ManifestImageResource, default }
