-- SPDX-License-Identifier: MPL-2.0
-- Copyright © 2020–2021 toastal dhall-webmanifest
{-
  https://www.w3.org/TR/screen-orientation/#dom-orientationlocktype
-}

let OrientationLock
    : Type
    = < any
      | natural
      | landscape
      | portrait
      | portrait-primary
      | portrait-secondary
      | landscape-primary
      | landscape-secondary
      >

in  { Type = OrientationLock }
