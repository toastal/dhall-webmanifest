-- An example with all optional fields empty
let WebAppManifest = ../src/WebAppManifest.dhall

in  WebAppManifest::{ name = "Empty" }
