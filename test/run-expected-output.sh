#!/usr/bin/env dash
set -euf

dir=$(dirname $(readlink -f "$0"))

for filepath in $(find $dir -name *.dhall); do
  file=$(basename $filepath)
  filename="${file%%.*}"

  echo -n "$file … "

  dhall_output="$(dhall-to-json --omit-empty --file $filepath)"
  expected_output="$(cat "${dir}/${filename}.expected.webmanifest")"

  if [ "$dhall_output" = "$expected_output" ]; then
    echo "passed."
  else
    echo "FAILED."
    echo
    echo "Expected output:"
    echo "${expected_output}"
    echo
    echo "But got:"
    echo "${dhall_output}"
    exit 1
  fi
done

exit 0
