#!/usr/bin/env dash
set -euf

dir=$(readlink -f "$(dirname $(readlink -f "$0"))/../src")
standard_schema="($dir/WebAppManifest.dhall).Type"

indent() {
  sed 's/^/      /'
}

read_webmanifest() {
  site="$1"
  uri="$2"
  schema="$3"
  json_to_dhall_opts=${4:-}
  echo -n ">>>>> Reading $site"
  if [ -n "$json_to_dhall_opts" ]; then
    echo -n " (with opts \`$json_to_dhall_opts\`)"
  fi
  echo
  # https://stackoverflow.com/questions/9112979/pipe-stdout-and-stderr-to-two-different-processes-in-shell-script#answer-31151808
  # Witchcraft; we’re not testing their servers or cURL but the file output
  # We’ll just `cat` cURL ‘errors’ to stdout
  # Considering this is gonna be run on a schedule, tests don’t need to fail
  # becuause a server went into maintenance
  #
  # swap stderr to stdout, stdout to std3
  # Then swap std3 back to stdout & parse JSON
  { curl --fail-early --show-error --silent --compressed --http2 -H "Content-Type: application/manifest+json" "$uri"  2>&1 1>&3 3>&- | cat; } \
    3>&1 1>&2 | json-to-dhall --plain "$schema" $json_to_dhall_opts \
    | indent
  echo
}

read_webmanifest \
  "PureScript Discourse" \
  "https://discourse.purescript.org//manifest.webmanifest" \
  "$standard_schema"

# url_template in ShareTarget.Params deprecated
read_webmanifest \
  "Mastodon.Social" \
  "https://mastodon.social/manifest.json" \
  "$standard_schema" \
  --records-loose

# url_template in ShareTarget.Params deprecated
read_webmanifest \
  "Twitter" \
  "https://twitter.com/manifest.json" \
  "$standard_schema" \
  --records-loose

read_webmanifest \
  "DuckDuckGo" \
  "https://duckduckgo.com/manifest.json" \
  "$standard_schema"

read_webmanifest \
  "React JS" \
  "https://reactjs.org/manifest.webmanifest" \
  "$standard_schema ⩓ { cacheDigest : Optional Text }"

# lol: mine is not a valid file without icons. TODO
#read_webmanifest \
#  "toast.al" \
#  "https://toast.al/manifest.webmanifest" \
#  "$standard_schema"
