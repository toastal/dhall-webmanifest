-- An example with all fields filled
let WebAppManifest = ../src/WebAppManifest.dhall

in    WebAppManifest::{
      , dir = Some WebAppManifest.TextDirection.Type.ltr
      , lang = Some "th-TH"
      , name = "ถั่วดาล"
      , short_name = Some "odtt"
      , description = Some "นี่คือการทดสอบ"
      , icons =
        [ { src = "odtt-icon.flif"
          , sizes = Some "69x69"
          , type = Some "image/flif"
          , purpose = Some "any"
          }
        , WebAppManifest.ManifestImageResource::{
          , src = "odtt-icon.ico"
          , sizes = Some "16x16 32x32"
          , purpose = Some
              ( WebAppManifest.ManifestImageResource.Purpose.concatShow
                  [ WebAppManifest.ManifestImageResource.Purpose.Type.any
                  , WebAppManifest.ManifestImageResource.Purpose.Type.maskable
                  ]
              )
          }
        , WebAppManifest.ManifestImageResource::{ src = "odtt-icon.svg" }
        ]
      , screenshots = Some
        [ { src = "odtt-scrot.apng"
          , sizes = Some "2560x1440"
          , type = Some "image/png"
          }
        ]
      , categories = Some [ "okay" ]
      , iarc_rating_id = None Text
      , start_url = Some "/"
      , display = Some WebAppManifest.DisplayMode.Type.minimal-ui
      , orientation = Some WebAppManifest.OrientationLock.Type.portrait-primary
      , theme_color = Some "#b71c1c"
      , background_color = Some "#000e53"
      , scope = Some "."
      , related_applications = Some
        [ { platform = "play"
          , url = Some
              "https://play.google.com/store/apps/details?id=com.example.app1"
          , id = Some "com.example.app1"
          , min_version = Some "2"
          , fingerprints = Some
            [ { type = "sha256_cert"
              , value = "92:5A:39:05:C5:B9:EA:BC:71:48:5F:F2"
              }
            ]
          }
        , WebAppManifest.ExternalApplicationResource::{
          , platform = "itunes"
          , url = Some "https://itunes.apple.com/app/example-app1/id123456789"
          }
        ]
      , prefer_related_applications = Some True
      , shortcuts = Some
        [ { name = "Blahblah"
          , short_name = Some "bb"
          , description = Some "lorem ipsum dolor"
          , url = "/"
          , icons = Some
            [ WebAppManifest.ManifestImageResource::{
              , purpose = Some
                  ( WebAppManifest.ManifestImageResource.Purpose.show
                      WebAppManifest.ManifestImageResource.Purpose.Type.monochrome
                  )
              , src = "odtt-icon.avif"
              }
            ]
          }
        , WebAppManifest.ShortcutItem::{ name = "pseudo", url = "/" }
        ]
      , share_target = Some
        { action = "/new-topic"
        , method = Some "POST"
        , enctype = Some "multipart/form-data"
        , params =
          { title = Some "TITLE"
          , text = Some "BODY once told me"
          , url = Some "https://discourse.purescript.org/"
          }
        }
      }
