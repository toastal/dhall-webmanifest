dhall-webmanifest
=================

:source-highlighter: pygments
:abbr-MDN: pass:[<abbr title="Mozilla Developer Network">MDN</abbr>]
:abbr-PWA: pass:[<abbr title="Progressive Web App">PWA</abbr>]

https://w3c.github.io/manifest/[Web App Manifest] in the https://dhall-lang.org/[Dhall] configuration language. The Web App Manifest enables progressive web apps ({abbr-PWA}s). For information about the Manifest’s compatibility, see https://developer.mozilla.org/en-US/docs/Web/Manifest#Browser_compatibility[browser compatibility] via {abbr-MDN}.

This project is available on https://gitlab.com/toastal/dhall-webmanifest[GitLab] and mirrored on https://git.sr.ht/~toastal/dhall-webmanifest[SourceHut] (though which is the mirror is subject to change).

== Usage

Create a file like `./webmanifest.dhall`

[source,dhall]
----
-- Pick a tag/commit hash! Freeze it too!
let WebAppManifest =
      https://gitlab.com/toastal/dhall-webmanifest/raw/trunk/src/WebAppManifest.dhall sha:…
    ? https://git.sr.ht/~toastal/dhall-webmanifest/blob/trunk/src/WebAppManifest.dhall sha:…

in    WebAppManifest::{
      , name = "toast.al"
      , lang = Some "en-US"
      , dir = Some WebAppManifest.TextDirection.Type.ltr
      , display = Some WebAppManifest.DisplayMode.Type.browser
      , orientation = Some WebAppManifest.OrientationLock.Type.natural
      , theme_color = Some "#f2008a"
      , background_color = Some "#f8f8f6"
      , categories = Some [ "blog", "programming" ]
      , icons =
        [ { src = "icon.avif"
          , sizes = Some "64x64"
          , type = Some "image/avif"
          , purpose = Some
              ( WebAppManifest.ManifestImageResource.Purpose.concatShow
                  [ WebAppManifest.ManifestImageResource.Purpose.Type.any
                  , WebAppManifest.ManifestImageResource.Purpose.Type.maskable
                  ]
              )
          }
        , { src = "favicon.ico"
          , sizes = Some "16x16 32x32"
          , type = None Text
          }
        , WebAppManifest.ImageResource::{ src = "icon.svg", purpose = Some "any" }
        ]
      }
    : WebAppManifest.Type
----
(sorry, no syntax highlighting on GitLab or SourceHut; https://github.com/rouge-ruby/rouge/issues/1054[`rouge` issue #1054])

Then to generate a minified `.webmanifest` (`Content-Type: application/manifest+json`) file for browser consumption

[source,console]
----
$ dhall-to-json --compact --omit-empty --file ./webmanifest.dhall --output ./manifest.webmanifest
----

== Test drive & verify

If you have `curl` and `json-to-dhall` installed on your `$PATH`, you can check the https://discourse.dhall-lang.org/[Dhall Discourse]’s Web App Manifest file.

[source,console]
----
$ curl --compressed "https://discourse.dhall-lang.org/manifest.json" \
  | json-to-dhall "(https://gitlab.com/toastal/dhall-webmanifest/raw/trunk/Webmanifest/WebAppManifest.dhall).Type"
----

'''

== Setup

=== Git

[source,console]
----
$ git config --local include.path ../.gitconfig
$ git config --local core.hooksPath .githooks
----

=== Nix + Flakes

==== For dev shell

[source,console]
----
$ nix develop
----

==== For automatic shell via `direnv`

[source,console]
----
$ direnv allow
----

'''

== License

This project is licensed under Mozilla Public License Version 2.0 - see the link:./LICENSE[./LICENSE] file for details.

== Funding

If you want to make a small contribution to the maintanence of this & other projects

:liberapay: https://liberapay.com/toastal/
:bitcoin-address: link:bitcoin://39nLVxrXPnD772dEqWFwfZZbfTv5BvV89y?message=Funding%20toastal%E2%80%99s%20dhall-contribute-json%20development
:bitcoin-verified: https://keybase.io/toastal/sigchain#690220ca450a3e73ff800c3e059de111d9c1cd2fcdaf3d17578ad312093fff2c0f
:zcash-address: link:zcash://t1a9pD1D2SDTTd7dbc15KnKsyYXtGcjHuZZ?message=Funding%20toastal%E2%80%99s%20dhall-contribute-json%20development
:zcash-verified: https://keybase.io/toastal/sigchain#65c0114a3c8ffb46e39e4d8b5ee0c06c9eb97a02c4f6c42a2b157ca83b8c47c70f

- {liberapay}[Liberapay]
- {bitcoin-address}[Bitcoin: `39nLVxrXPnD772dEqWFwfZZbfTv5BvV89y`] (verified on {bitcoin-verified}[Keybase])
- {zcash-address}[Zcash: `t1a9pD1D2SDTTd7dbc15KnKsyYXtGcjHuZZ`] (verified on {zcash-verified}[Keybase])
