{
  description = "dhall-webmanifest";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        name = "dhall-webmanifest";

        pkgs = import nixpkgs { inherit system; };

        nativeBuildInputs = with pkgs; [
          dash
          dhall
          dhall-json
          fish
          jq
        ];
      in
      rec {
        packages = {
          readme = pkgs.stdenv.mkDerivation {
            name = "readme";
            description = "Render README";
            src = self;
            nativeBuildInputs = nativeBuildInputs;
            buildInputs = with pkgs; [
              asciidoctor
            ];
            unpackPhase = "true";
            preferLocalBuild = true;
            buildPhase = ''
              readmeDir="$out/readme"
              mkdir -p $readmeDir
              asciidoctor \
                --backend html5 \
                --embedded \
                --attribute nofooter \
                --destination-dir=$readmeDir \
                --out-file=$readmeDir/README.html \
                $src/README.adoc
              cp $src/LICENSE $readmeDir
            '';
            dontInstall = true;
          };

          ${name} = pkgs.stdenv.mkDerivation {
            inherit name;
            src = self;
          };
        };

        defaultPackage = packages.${name};

        devShell = pkgs.mkShell {
          inherit name nativeBuildInputs;
          inputsFrom = builtins.attrValues self.packages.${system};
          buildInputs = packages.readme.buildInputs;
        };
      }
    );
}
