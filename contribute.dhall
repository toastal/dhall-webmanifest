let Contribute =
      https://git.sr.ht/~toastal/dhall-contribute-json/blob/trunk/src/Contribute.dhall
        sha256:f35b5217941dc6878b37eda73cc553cf16dde1d2c83c1256adb487c82beb4837

in  { name = "dhall-webmanifest"
    , description = "Web App Manifest for the Dhall configuration language"
    , repository = Contribute.Repository::{
      , license = "MPL-2.0"
      , url = "https://gitlab.com/toastal/dhall-webmanifest"
      , type = Some "git"
      , clone = Some "git@gitlab.com:toastal/dhall-webmanifest.git"
      }
    , bugs = Some Contribute.Bugs::{
      , list = Some "https://gitlab.com/toastal/dhall-webmanifest/issues"
      , report = Some "https://gitlab.com/toastal/dhall-webmanifest/issues/new"
      }
    , keywords = Some [ "dhall", "json", "webmanifest", "web-app-manifest" ]
    }
